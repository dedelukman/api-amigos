package com.example.demo.student;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StudentService {


    private final StudentRepository studentRepository;

    public List<Student> GetStudent(){
        return studentRepository.findAll();
    }

    public void addNewStudent(Student student) {
        Optional<Student> studentByEmail = studentRepository.findStudentByEmail(student.getEmail());
        if (studentByEmail.isPresent()){
            throw new IllegalMonitorStateException("Email taken");
        }
        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        boolean exists = studentRepository.existsById(studentId);
        if (!exists){
            throw new IllegalStateException("Student with id : " + studentId + " is not exist");
        }
        studentRepository.deleteById(studentId);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateStudent(Long studentId, String name, String email) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new IllegalStateException(
                        "Student with id : " + studentId + " is not exist"
                ));
        if (name != null &&
            name.length() > 0 &&
            !Objects.equals(student.getName(), name)){
            student.setName(name);
        }

        if (email != null &&
        email.length() > 0 &&
                !Objects.equals(student.getEmail(), email)){
            Optional<Student> studentByEmail = studentRepository.findStudentByEmail(email);
            if (studentByEmail.isPresent()){
                throw new IllegalMonitorStateException("Email taken");
            }
            student.setEmail(email);
        }
        System.out.println(name +' ' +email);
    }
}
