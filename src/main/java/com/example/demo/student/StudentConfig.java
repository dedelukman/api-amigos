package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository){
        return args -> {
            Student ibrahim = new Student(
                    "Ibrahim",
                    "Ibrahim@abahstudio.com",
                    LocalDate.of(2020, Month.SEPTEMBER, 20)

            );
            Student lukman = new Student(
                    "Lukman",
                    "Abah@abahstudio.com",
                    LocalDate.of(1986, Month.AUGUST, 24)

            );
            studentRepository.saveAll(List.of(ibrahim, lukman ));
        };
    }
}
