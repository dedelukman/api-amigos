package com.example.demo.student;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table
@Getter@Setter
@NoArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_sequence")
    @SequenceGenerator(name = "student_sequence", sequenceName = "student_sequence", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @NonNull
    private String name;
    @NonNull
    private String email;
    private LocalDate dob;
    @Transient
    private int age;

    public int getAge() {
        return Period.between(getDob(), LocalDate.now()).getYears();
    }

    public Student(@NonNull String name, @NonNull String email, LocalDate dob) {
        this.name = name;
        this.email = email;
        this.dob = dob;
    }
}
